import pickle
import matplotlib.pyplot as plt
import numpy as np
from sklearn.model_selection import train_test_split
from keras.preprocessing.text import Tokenizer, text_to_word_sequence
from sklearn.preprocessing import LabelBinarizer

from keras.models import Sequential
from keras.layers import Activation, Dense
import pandas as pd
from sklearn.metrics import classification_report, confusion_matrix, accuracy_score, ConfusionMatrixDisplay

from tensorflow.python.keras.callbacks import ModelCheckpoint, LearningRateScheduler


def get_dataset(data_events):
    """
    This function gets the data events pathname for the teacher model and reads the contents
    :param data_events: pathname with the data for training
    """
    logfile = data_events
    train_data = pd.read_csv(logfile, names=['input_IP_address', 'Destination_IP_address',
                                             'Description_cleaned', 'Priority'], sep=",",
                             header=None,
                             nrows=299999)
    num_classes = max(train_data.Priority)
    return train_data, num_classes


def get_STD_dataset(data_events):
    """
    This function gets the data events pathname for the student model and reads the contents
    :param data_events: pathname with the data for training
    """
    logfile = data_events
    train_data = pd.read_csv(logfile, names=['input_IP_address', 'Destination_IP_address',
                                             'Description_cleaned', 'Priority'], sep=",",
                             header=None,
                             skiprows=299999,
                             nrows=29999)
    num_classes = max(train_data.Priority)
    return train_data, num_classes


def prepare_std_data(text):
    tokenizer = Tokenizer(filters='!"#$%&()*+,-./:;<=>?@[\\]^_`{|}~\t\n',
                          split=" ", num_words=20)
    tokenizer.fit_on_texts(text)
    X_tfidf_resampled = tokenizer.texts_to_matrix(text, mode='tfidf')
    return X_tfidf_resampled


def prepare_data(data):
    """
    This function creates a tokenizer to transform string data into float. Every tokenizer is unique for a set of
    data therefore, it can only be used for the specific data or others that have characters of the same vocabulary.
    :param data: Data size
    """
    tokenizer = Tokenizer(filters='!"#$%&()*+,-./:;<=>?@[\\]^_`{|}~\t\n',
                          split=" ", num_words=20)
    X_train, X_test, y_train, y_test = train_test_split(data.Description_cleaned, data.Priority, test_size=0.01,
                                                        random_state=42)
    tokenizer.fit_on_texts(X_train)
    # comments
    X_tfidf_resampled = tokenizer.texts_to_matrix(X_train, mode='tfidf')
    X_test = tokenizer.texts_to_matrix(X_test, mode='tfidf')
    encoder = LabelBinarizer()
    encoder.fit(data.Priority)
    Y_tfidf_resampled = encoder.transform(y_train)
    y_test = encoder.transform(y_test)
    # comment the process
    # saving
    with open('tokenizer.pickle', 'wb') as handle:
        pickle.dump(tokenizer, handle, protocol=pickle.HIGHEST_PROTOCOL)
    return X_tfidf_resampled, X_test, Y_tfidf_resampled, y_test


def lr_schedule(epoch):
    """
    currently not used. function for the learning rate decay.
    """
    lr = 0.01
    return lr * (0.1 ** int(epoch / 10))


def load_model_from_checkpoint(model_path, input_size, hidden_size, num_classes):
    """
    This function loads the model from the last saved checkpoint
    :param model_path: Model path
    :param input_size: Size of the data
    :param hidden_size: Size of the hidden layer
    :param num_classes: Number of classes
    """
    model = build_nn(input_size, hidden_size, num_classes)
    model.load_weights(model_path)
    return model


def build_nn(input_size, hidden_size, num_classes):
    """
    This function defines the configuration parameters for the network
    :param input_size: Size of the data
    :param hidden_size: Size of the hidden layer
    :param num_classes: Number of classes
    """
    model = Sequential()
    model.add(Dense(hidden_size, input_dim=input_size))
    model.add(Dense(128))
    model.add(Activation('tanh'))
    model.add(Dense(64))
    model.add(Dense(num_classes))
    model.add(Activation('softmax'))

    model.summary()
    return model


def get_model_config(training_data):
    """
    This function defines the configuration parameters for the network
    :param training_data: Data for training
    """
    input_size = training_data.shape[1]  # this is the vocab size
    hidden_size = 512
    dropout = 0.0

    num_epochs = 20
    batch_size = 256
    learning_rate = 0.0005

    return input_size, hidden_size, dropout, num_epochs, batch_size, learning_rate


def get_model_config_std(training_data):
    """
    This function defines the configuration parameters for the network
    :param training_data: Data for training
    """
    input_size = training_data.shape[1]  # this is the vocab size
    hidden_size = 512
    dropout = 0.0

    num_epochs = 50
    batch_size = 256
    learning_rate = 0.0005

    return input_size, hidden_size, dropout, num_epochs, batch_size, learning_rate


def train(model, batch_size, num_epochs, training_data, label_data, checkpoint_path):
    """
    This function executes the training of the neural network through the fit function.
    :param model: Model entity for training
    :param batch_size: Batch size of the data
    :param training_data: Data for training
    :param label_data: Labels of the data
    :param checkpoint_path: Checkpoint path for saving
    return: Accuracy score
    """
    checkpoint = ModelCheckpoint(filepath=checkpoint_path, monitor='loss', verbose=1, save_best_only=True, save_freq=10,
                                 mode='min')
    callbacks_list = [LearningRateScheduler(lr_schedule), checkpoint]
    class_weight = {0: 35.,
                    1: 1.,
                    2: 1.}

    history = model.fit(training_data, label_data,
                        batch_size=batch_size,
                        epochs=num_epochs,
                        verbose=1,
                        shuffle=True,
                        callbacks=callbacks_list,
                        class_weight=class_weight)

    return model, history


def report(actual, predictions):
    """
    This function prepares a report for the training. It calculates the confusion matrix and other metrics.
    :param actual: Actual values of the data, ground truth
    :param predictions: Predictions from the training
    return: Accuracy score
    """
    print("\033[1m Performance Report \033[0m\033[50m\n")

    actual = actual.tolist()
    predictions = predictions.tolist()
    actual = np.array(actual)
    predictions = np.array(predictions)

    print(confusion_matrix(actual.argmax(1), predictions.argmax(1)))
    print(classification_report(actual, predictions))
    print("Accuracy: " + str(round(accuracy_score(actual, predictions), 2)))
    disp = ConfusionMatrixDisplay(confusion_matrix=confusion_matrix(actual.argmax(1), predictions.argmax(1)))

    disp.plot(cmap=plt.cm.Blues)
    plt.show()
    return round(accuracy_score(actual, predictions))


def softmax_preds(model, Data_for_test, Labels_for_test, encoder):
    """
    This function calculates the accuracy
    :param model: number of teachers (in the ensemble) to learn from
    :param Data_for_test: Testing data
    :param Labels_for_test: Labels of the testing data
    :param encoder: Data encoder for converting label values
    :return: Accuracy results
    """
    # print(np.array(X_test).shape)
    result = model.predict(np.array(Data_for_test), batch_size=256)
    predicted_labels = encoder.inverse_transform(result)

    # Reporting
    result_acc = report(Labels_for_test, predicted_labels)

    return result_acc
