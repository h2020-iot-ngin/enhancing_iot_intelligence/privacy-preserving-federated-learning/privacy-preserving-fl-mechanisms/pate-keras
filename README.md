# PATE-Keras

Implementation of Private Aggregation of Teacher Ensembles (PATE) in Keras

In this example, a sequential ML model for classification is implemented and tested on Suricata dataset collected from Synelixis PfSense platform. 

**Model Architecture**

	Model: "sequential"
	Layer (type)                 Output Shape              Param #   
	=================================================================
	dense (Dense)                (None, 512)               10752     
	_________________________________________________________________
	dense_1 (Dense)              (None, 128)               65664     
	_________________________________________________________________
	activation (Activation)      (None, 128)               0         
	_________________________________________________________________
	dense_2 (Dense)              (None, 64)                8256      
	_________________________________________________________________
	dense_3 (Dense)              (None, 3)                 195       
	_________________________________________________________________
	activation_1 (Activation)    (None, 3)                 0         
	================================================================= 

# Learning private models with multiple teachers

This repository contains code to create a setup for learning privacy-preserving
student models by transferring knowledge from an ensemble of teachers trained
on disjoint subsets of the data for which privacy guarantees are to be provided.

Knowledge acquired by teachers is transferred to the student in a differentially
private manner by noisily aggregating the teacher decisions before feeding them
to the student during training.

# How to run

-Clone the repository

-Install the requirements from requirements.txt

-Execute the command: python pate_orchestrator.py -i input_dir --t number_of_teachers --data data_input
