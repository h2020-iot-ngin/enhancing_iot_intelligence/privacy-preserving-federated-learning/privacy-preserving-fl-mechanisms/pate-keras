# Pate + Flower Orchestrator
import argparse

import train_teachers
import train_student


def main():
    """
    # train_teachers.train_mp_teachers(nb_teachers, train_dir, data_events, label_events)

    :param nb_teachers: number of teachers (in the ensemble) to learn from
    :param train_dir: Training directory
    :param data_events: Data for training with the corresponding labels
    :return: True if everything went well
    # train_student.train_student_job(nb_teachers, num_classes, train_dir, data_events, label_events)

    :param nb_teachers: number of teachers (in the ensemble) to learn from
    :param num_classes: Number of classes for the ML classification
    :param train_dir: Training directory
    :param data_events: Data for training with the corresponding labels
    :return: True if everything went well
    """
    parser = argparse.ArgumentParser()
    # Add an argument

    parser = argparse.ArgumentParser(description="Flower")
    parser.add_argument(
        "--i",
        type=str,
        required=True,
        help=f"Training directory",
    )
    parser.add_argument(
        "--t",
        type=int,
        required=True,
        help="Number of Teachers for PATE implementation",
    )
    parser.add_argument(
        "--data",
        type=str,
        required=True,
        help=f"Data for training with the corresponding labels - CSV file",
    )

    args = parser.parse_args()
    train_dir = args.i
    nb_teachers = args.t
    data_events = args.data

    train_teachers.train_mp_teachers(nb_teachers, train_dir, data_events)
    # the below function should run on the server
    train_student.train_student_job(nb_teachers, train_dir, data_events)


if __name__ == "__main__":
    main()
