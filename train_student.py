from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import pickle
import numpy as np
from tensorflow.python.keras.optimizers import Adam

from tensorflow.keras.optimizers import Adam
from sklearn.preprocessing import LabelBinarizer

import aggregation
import train_NN_for_PATE
import tensorflow as tf

tf.compat.v1.disable_eager_execution()


def ensemble_preds(nb_teachers, student_data, num_classes, train_dir):
    """
    Given a dataset, a number of teachers, and some input data, this helper
    function queries each teacher for predictions on the data and returns
    all predictions in a single array.
    :param nb_teachers: number of teachers (in the ensemble) to learn from
    :param student_data: student training data
    :param num_classes: Number of Classes
    :param train_dir: Training directory
    :return: 3d array (teacher id, sample id, probability per class)
    """

    # Compute shape of array that will hold probabilities produced by each
    # teacher, for each training point, and each output class
    # loading

    # comments
    with open('tokenizer.pickle', 'rb') as handle:
        tokenizer = pickle.load(handle)

    student_labels = student_data.drop(columns=['Priority'], axis=1)
    # data_temp = df_data_temp.astype(str).values.tolist()
    # convert student labels through tokenization
    tokenized_student_data = tokenizer.texts_to_matrix(student_labels.Description_cleaned, mode='tfidf')
    # compute array shape of the predictions
    predictions_array_shape = (nb_teachers, len(tokenized_student_data), num_classes)

    # Create array that will hold result for the predictions
    predictions = np.zeros(predictions_array_shape, dtype=np.float32)

    # Get predictions from each teacher
    # save model to json and reload
    for teacher_id in range(nb_teachers):
        # Compute path of weight file for teacher model with ID teacher_id
        filename = train_dir + '/' + str(nb_teachers) + '_teachers_' + str(teacher_id) + '.h5'
        # Get model configuration and reload teacher weights
        input_size, hidden_size, dropout, num_epochs, batch_size, learning_rate = \
            train_NN_for_PATE.get_model_config_std(tokenized_student_data)
        # load model from last checkpoint of a teacher model
        model = train_NN_for_PATE.load_model_from_checkpoint(filename, input_size, hidden_size, num_classes)
        # Get predictions on our training data and store in result array
        model.summary()
        encoder = LabelBinarizer()
        # predict using teacher model based on student "public" data
        predictions[teacher_id] = model.predict(np.array(tokenized_student_data), batch_size=256)

        # This can take a while when there are a lot of teachers so output status
        # print("Computed Teacher " + str(teacher_id) + "predictions")

    return predictions, tokenized_student_data


def prepare_student_data(train_data, nb_teachers, lap_scale, num_classes, train_dir):
    """
    Takes a dataset name and the size of the teacher ensemble and prepares
    training data for the student model
    :param train_data: Data for training
    :param nb_teachers: number of teachers (in the ensemble) to learn from
    :param: lap_scale: scale of the Laplacian noise added for privacy
    :param: num_classes: Number of Classes for Classification (ML)
    :param train_dir: Training directory
    :return: pairs of (data, labels) to be used for student training and testing
    """

    # Compute teacher predictions for student training data
    # comment change names
    teachers_predictions, tokenized_student_data = ensemble_preds(nb_teachers, train_data, num_classes, train_dir)

    # Aggregate teacher predictions to get student training labels
    student_labels = aggregation.noisy_max(teachers_predictions, lap_scale)

    # Store unused part of test set for use as a test set after student training

    return student_labels, tokenized_student_data


def train_student_job(nb_teachers, train_dir, data_events):
    """
    This function trains a student using predictions made by an ensemble of
    teachers. The student and teacher models are trained using the same
    neural network architecture.
    :param nb_teachers: number of teachers (in the ensemble) to learn from
    :param train_dir: Training directory
    :param data_events: Data for training with the corresponding labels
    :return: True if student training went well
    """
    # you need to change the address of get_dataset() manually
    lap_scale = 3
    train_data, num_classes = train_NN_for_PATE.get_STD_dataset(data_events)

    # Call helper function to prepare student data using teacher predictions
    student_labels, tokenized_student_data = prepare_student_data(train_data, nb_teachers, lap_scale, num_classes,
                                                                  train_dir)
    # comment change name
    model_checkpoint_name_hdf5 = train_dir + '/student_' + str(11) + '.hdf5'
    model_checkpoint_name_h5 = train_dir + '/student_' + str(11) + '.h5'

    """
    this function gets student's model configuration
    param input_size: Size of the input data
    param hidden_size: Size of the hidden layer
    param num_classes: Number of Classes
    param dropout: Dropout value for the NN
    param num_epochs: Number of training epochs
    param batch_size: Size of the batch for the NN
    param learning_rate: Learning rate value
    """
    input_size, hidden_size, dropout, num_epochs, batch_size, learning_rate = \
        train_NN_for_PATE.get_model_config_std(tokenized_student_data)

    # Create student model
    model = train_NN_for_PATE.build_nn(input_size, hidden_size, num_classes)
    criterion = 'categorical_crossentropy'
    opt = Adam(lr=learning_rate)
    model.compile(loss=criterion,
                  optimizer=opt,
                  metrics=['accuracy'])
    encoder = LabelBinarizer()
    encoder.fit(student_labels)
    student_labels = encoder.transform(student_labels)
    model, hist = train_NN_for_PATE.train(model, batch_size, num_epochs, tokenized_student_data, student_labels,
                                          model_checkpoint_name_h5)
    # Compute final checkpoint name for student
    model.save_weights(model_checkpoint_name_hdf5)

    return True
